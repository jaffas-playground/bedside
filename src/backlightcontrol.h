#ifndef BACKLIGHTCONTROL_H
#define BACKLIGHTCONTROL_H

#include <QObject>
#include <QProcess>
#include <QDebug>
#include <QSettings>
#include <qplatformdefs.h>

/**
 * Simple QObject which supports dimming the device's backlight.
 */
class BacklightControl : public QObject
{
    Q_OBJECT

private:
    int _value;
    bool _dimmed;

    /**
     * Set the value to the given integer.
     */
    void setValue(int value) {
        #if defined(MEEGO_EDITION_HARMATTAN) || defined(Q_WS_MAEMO_5)
            QProcess gconftool;
            QString num;

            // Unlike Fremantle, Harmattan only goes down to 1
            #ifdef MEEGO_EDITION_HARMATTAN
                qDebug() << "Harmattan value = " << value;
                if (value < 1)
                    value = 1;
            #endif

            num.setNum(value);
            QStringList args = QStringList() << "-s" << "/system/osso/dsm/display/display_brightness" << "-t" << "int" << num;
            qDebug() << "Resetting brightness: " << args;
            gconftool.start("gconftool-2", args);
            gconftool.waitForFinished();
        #endif
    }


    /**
     * Get the current value.
     */
    int getValue() {
        int result = 3;
        #if defined(MEEGO_EDITION_HARMATTAN) || defined(Q_WS_MAEMO_5)
            QProcess gconftool;
            gconftool.start("gconftool-2", QStringList() << "-g" << "/system/osso/dsm/display/display_brightness");
            if (gconftool.waitForFinished()) {
                result = QString(gconftool.readAll()).toInt();
                qDebug() << "Current brightness [" << result << "]";
            }
        #endif

        return result;
    }

public:
    BacklightControl(QObject *parent) : QObject(parent) {
        QSettings settings("jaffa","Bedside");
        _value = getValue();
        _dimmed = settings.value("general/dimmed", false).toBool();
        setDimmed(_dimmed);
    }

public slots:
    /**
     * Set the dim state.
     */
    void setDimmed(const bool t) {
        if (t == _dimmed) {
            return;

        } else if (_dimmed) {
            _dimmed = false;
            setValue(_value);

        } else {
            _value = getValue();
            _dimmed = true;
            setValue(0);
        }
    }

    /**
     * Reset to user-controlled brightness.
     */
    void reset() {
        setDimmed(false);
    }

signals:
};


#endif // BACKLIGHTCONTROL_H
