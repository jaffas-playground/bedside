#ifndef MAIN_H
#define MAIN_H

#include <QSettings>
#include <QColor>
#include <QDebug>
#include <qplatformdefs.h>

#ifdef MEEGO_EDITION_HARMATTAN
  #include <MLocale>
#elif defined(Q_OS_BLACKBERRY)
  #include <bb/pim/calendar/CalendarService>
  #include <bb/pim/calendar/CalendarSettings>
#endif

class ApplicationData : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int colorNum READ colorNum WRITE setColorNum NOTIFY colorNumChanged)
    Q_PROPERTY(QColor color READ color NOTIFY colorChanged)
    Q_PROPERTY(bool dimmed READ dimmed WRITE setDimmed NOTIFY dimmedChanged)
public:
    void initialize() {
        QSettings settings("jaffa","Bedside");
        m_colorNum = settings.value("colorNum",0).toInt();
        m_dimmed = settings.value("dimmed",false).toBool();
    }

    bool twelveHourClock() {
        bool ampm = false;

        #ifdef MEEGO_EDITION_HARMATTAN
            MLocale locale;
            MLocale::TimeFormat24h timeFormat = locale.timeFormat24h();
            if (timeFormat == MLocale::LocaleDefaultTimeFormat24h)
                timeFormat = locale.defaultTimeFormat24h();

            ampm = timeFormat == MLocale::TwelveHourTimeFormat24h;

        #elif defined(Q_OS_BLACKBERRY)
            bb::pim::calendar::CalendarService calendarService;
            ampm = !calendarService.settings().is24HourFormat();

        #else
            QLocale locale;
            ampm = locale.timeFormat().indexOf("AP") != -1;
        #endif
        qDebug() << "Twelve hour clock = " << ampm;

        return ampm;
    }

    int colorNum() {
        return m_colorNum;
    }

    QColor color() {
        QColor c;
        switch (m_colorNum)
        {
        case 1:
            c = Qt::cyan;
            break;
        case 2:
            c = Qt::blue;
            break;
        case 3:
            c = Qt::magenta;
            break;
        case 4:
            c = Qt::red;
            break;
        case 5:
            c = QColor("orange");
            break;
        case 6:
            c = Qt::white;
            break;
        default:
            c = Qt::green;
            break;
        }
	return c;
    }

    bool dimmed() {
        return m_dimmed;
    }

    void setColorNum(const int color) {
        if (color != m_colorNum) {
            QSettings settings("jaffa","Bedside");
            m_colorNum = color;
            settings.setValue("colorNum", color);
            emit colorNumChanged();
            emit colorChanged();
        }
    }

    void setDimmed(const bool dim) {
        if (dim != m_dimmed) {
            QSettings settings("jaffa","Bedside");
            m_dimmed = dim;
            settings.setValue("dimmed", dim);
            emit dimmedChanged();
        }
    }
        
signals:
    void colorNumChanged();
    void colorChanged();
    void dimmedChanged();
private:
    int m_colorNum;
    bool m_dimmed;
};

#endif //MAIN_H
