#include <QtGui/QApplication>
#include <QtDeclarative>
#include "visibilityawareqmlapplicationviewer.h"
#include "activemonitor.h"
#include "backlightcontrol.h"
#include "applicationdata.h"
#include <qplatformdefs.h>

#include <QDebug>


void myMessageOutput(QtMsgType type, const char* msg){
               fprintf(stdout, "%s\n", msg);
               fflush(stdout);
}

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    qInstallMsgHandler(myMessageOutput);

    qDebug() << "Initialising Bedside...";
    ApplicationData data;
    data.initialize();

    VisibilityAwareQmlApplicationViewer viewer;
    viewer.engine()->addImportPath("/opt/qtm12/imports");
    viewer.engine()->addImportPath("/opt/Bedside");

#ifndef Q_OS_BLACKBERRY
    viewer.setOrientation(QmlApplicationViewer::ScreenOrientationAuto);
#endif

    ActiveMonitor *monitor = new ActiveMonitor(&viewer);
    QObject::connect(&viewer, SIGNAL(activeChanged(bool)),
                     monitor, SLOT(activeChanged(bool)));

    BacklightControl *backlight = new BacklightControl(&viewer);
    QObject::connect(&app, SIGNAL(aboutToQuit()), backlight, SLOT(reset()));

    viewer.rootContext()->setContextProperty("activeMonitor", monitor);
    viewer.rootContext()->setContextProperty("backlight", backlight);
    viewer.rootContext()->setContextProperty("viewer", &viewer);
    viewer.rootContext()->setContextProperty("applicationData", &data);
    viewer.rootContext()->setContextProperty("ampm", data.twelveHourClock());
    viewer.rootContext()->setContextProperty("screenScale", QApplication::desktop()->logicalDpiX() / 25.4);

    qDebug() << "Screen DPI: " << QApplication::desktop()->logicalDpiX();

    #ifdef MEEGO_EDITION_HARMATTAN
    	qDebug() << "Detected platform: Harmattan";
        viewer.rootContext()->setContextProperty("needCloseIcon", "false"); // `false' bool comes through as `null'
        viewer.setSource(QUrl("qrc:///assets/qml/Bedside/qtc-root.qml"));
    #elif defined(Q_OS_BLACKBERRY)
    	qDebug() << "Detected platform: BlackBerry";
        viewer.rootContext()->setContextProperty("needCloseIcon", "false"); // `false' bool comes through as `null'
        viewer.setSource(QUrl("qrc:///assets/qml/Bedside/BB10-root.qml"));
	#elif defined(Q_OS_ANDROID)
    	qDebug() << "Detected platform: Android";
        viewer.rootContext()->setContextProperty("needCloseIcon", "true");
        viewer.setSource(QUrl("qrc:///assets/qml/Bedside/Main.qml"));
    #else
    	qDebug() << "Detected platform: <none>";
        viewer.rootContext()->setContextProperty("needCloseIcon", "true");
        viewer.setSource(QUrl("qrc:///assets/qml/Bedside/mobility-root.qml"));
    #endif
    viewer.showFullScreen();

    qDebug() << "Initialisation complete";
    return app.exec();
}
