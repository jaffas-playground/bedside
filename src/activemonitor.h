#ifndef _ACTIVEMONITOR_H
#define _ACTIVEMONITOR_H

#include <QObject>
#include <QDebug>
#include <qplatformdefs.h>

#ifdef MEEGO_EDITION_HARMATTAN
    #include <QDBusInterface>
#elif defined(Q_OS_BLACKBERRY)
    #include <bps/bps.h>
    #include <bps/navigator.h>
    #include <bps/screen.h>
    #include <errno.h>
    #include <unistd.h>
#endif

/**
 * Simple QObject with a single NOTIFYable, read-only property
 * that can be used inside QML to read application active status.
 *
 * The slot activeChanged(bool) should be connected to the
 * activeChanged(bool) signal of the modified QmlApplicationViewer.
 */
class ActiveMonitor : public QObject
{
    Q_OBJECT

private:
    bool _active;
    bool _visible;
    bool _external;

#ifdef Q_OS_BLACKBERRY
	bool checkBpsState(int state, const char* message) {
		if (BPS_FAILURE != state)
			return true;

		qDebug() << "[BPS] State = " << state << ", err = " << errno << ": " << message;
		bps_shutdown();
		return false;
	}
#endif


public:
    ActiveMonitor(QObject *parent) : QObject(parent) {}
    bool visible() { return _visible; }
    void setVisible(bool visible) {
        if (_visible != visible) {
            _visible = visible;
            emit changed();
        }
    }

    bool active() { return _active; }
    void setActive(bool active) {
        if (_active != active) {
            _active = active;
            emit changed();
        }
    }

    bool external() { return _external; }
    void setExternal(bool external) {
        _external = external;
    }

    Q_PROPERTY(bool active READ active WRITE setActive NOTIFY changed)
    Q_PROPERTY(bool visible READ visible WRITE setVisible NOTIFY changed)
    Q_PROPERTY(bool external READ external WRITE setExternal)

public slots:
    void activeChanged(bool active) {
        if (!_external) {
            _visible = active;
            setActive(active);
        }

        // -- Workaround http://www.developer.nokia.com/bugs/show_bug.cgi?id=592...
        //
        #if defined(MEEGO_EDITION_HARMATTAN) && !defined(QT_NO_DBUS)
            if (!active) {
                qDebug() << "Firing req_display_cancel_blanking_pause due to DNB#592";
                QDBusInterface *mceConnectionInterface = new QDBusInterface("com.nokia.mce",
                                                                            "/com/nokia/mce/request",
                                                                            "com.nokia.mce.request",
                                                                            QDBusConnection::systemBus(), this);
                if (mceConnectionInterface->isValid())
                    mceConnectionInterface->call("req_display_cancel_blanking_pause");
            }
        #endif // QT_NO_DBUS
    }


    void updateCover() {
#ifdef Q_OS_BLACKBERRY

        int state;
        navigator_window_cover_attribute_t* attr;

        // set-up the BPS by initializing it.
        state = bps_initialize();
        if (!checkBpsState(state, "Initialisation"))
            return;

        state = navigator_get_device_lock_state();
        if (!checkBpsState(state, "Device lock state")) {
            return;
        } else if (state != NAVIGATOR_DEVICE_LOCK_STATE_UNLOCKED) {
            if (_visible) {
                qDebug() << "Changing to not visible";
                _visible = false;
                emit changed();
            }
        } else {
            if (!_visible) {
                qDebug() << "Changing to visible";
                _visible = true;
                emit changed();
            }
        }

        // initialization was successful,
        // create a window cover attribute structure
        // this will be used when updating the cover
        state = navigator_window_cover_attribute_create(&attr);
        if (!checkBpsState(state, "Create cover attribute"))
            return;

        // the window cover attribute was created ok,
        // use that structure to signal the window cover to be updated
        if (_visible) {
            qDebug() << "Updating cover...";
            state = navigator_window_cover_attribute_set_live(attr);
            if (!checkBpsState(state, "Setting live"))
                return;
        } else {
            qDebug() << "Fixing cover...";
            state = navigator_window_cover_attribute_set_file(attr, "app/native/images/cover.png");
            if (!checkBpsState(state, "Fix cover"))
                return;
        }

        state = navigator_window_cover_update(attr);
        if (!checkBpsState(state, "Update cover"))
            return;

        // now that the cover has been updated
        // free up the area of memory held by the cover attribute structure
        state = navigator_window_cover_attribute_destroy(attr);
        if (!checkBpsState(state, "Destroy cover attribute"))
            return;

        // shutdown the BPS and free up and memory that may have been allocated.
        bps_shutdown();
#endif
    }

signals:
    void changed();
};

#endif
