#ifndef VISIBILITYAWAREQMLAPPLICATIONVIEWER_H
#define VISIBILITYAWAREQMLAPPLICATIONVIEWER_H
#include "qmlapplicationviewer.h"

class VisibilityAwareQmlApplicationViewer : public QmlApplicationViewer
{
    Q_OBJECT
public:
    explicit VisibilityAwareQmlApplicationViewer(QWidget *parent = 0);
    bool event(QEvent *event);

signals:
    /* Support for application active signals (see ActiveMonitor) */
    void activeChanged(bool active);

public slots:
    /* Support for minimising the application to the task switcher */
    void minimise();
};

#endif // VISIBILITYAWAREQMLAPPLICATIONVIEWER_H
