#include "visibilityawareqmlapplicationviewer.h"
#include "qmlapplicationviewer.h"
#if defined(Q_WS_MAEMO_5)
#include <QDBusConnection>
#include <QDBusMessage>
#endif

VisibilityAwareQmlApplicationViewer::VisibilityAwareQmlApplicationViewer(QWidget *parent) :
    QmlApplicationViewer(parent)
{
}

bool
VisibilityAwareQmlApplicationViewer::event(QEvent *event)
{
    switch (event->type()) {
        case QEvent::Leave:
        case QEvent::WindowDeactivate:
            /* Window was sent to the background */
            emit activeChanged(false);
            break;
        case QEvent::Enter:
        case QEvent::WindowActivate:
            /* Window is again in the foreground */
            emit activeChanged(true);
            break;
        default:
            break;
    }

    /* Handle all events by the parent class */
    return QDeclarativeView::event(event);
}

void
VisibilityAwareQmlApplicationViewer::minimise()
{
    #if defined(Q_WS_MAEMO_5)
        QDBusConnection c = QDBusConnection::sessionBus();
        QDBusMessage m = QDBusMessage::createSignal("/", "com.nokia.hildon_desktop", "exit_app_view");
        c.send(m);
    #endif
}
