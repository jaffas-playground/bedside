VERSION = 1.2.1
TARGET = Bedside

# Add more folders to ship with the application, here
folder_01.source = qml/Bedside
folder_01.target = qml

folder_02.source = images
folder_02.target =

file_03.source = Bedside.png
file_03.target = images/
DEPLOYMENTFOLDERS = folder_01 folder_02 file_03

# Additional import path used to resolve QML modules in Creator's code model
QML_IMPORT_PATH =

#symbian:TARGET.UID3 = 0x2004B852 # Allocated by Nokia
symbian:TARGET.UID3 = 0xE06E41A9 # For testing

unix:symbian:maemo5 {
  CONFIG += mobility11
  MOBILITY += systeminfo sensors
}

device|simulator {
  CONFIG += qt warn_on debug_and_release cascades
  LIBS += -lbbpim
}

device {
        CONFIG(release, debug|release) {
                DESTDIR = o.le-v7
        }
        CONFIG(debug, debug|release) {
                DESTDIR = o.le-v7-g
        }
}

simulator {
        CONFIG(release, debug|release) {
                DESTDIR = o
        }
        CONFIG(debug, debug|release) {
                DESTDIR = o-g
        }
}

INCLUDEPATH += src
SOURCES += src/main.cpp
HEADERS += src/activemonitor.h \
    src/applicationdata.h \
    src/backlightcontrol.h \

# Please do not modify the following two lines. Required for deployment.
include(qmlapplicationviewer/qmlapplicationviewer.pri)
qtcAddDeployment()

OTHER_FILES += \
    qtc_packaging/debian_harmattan/rules \
    qtc_packaging/debian_harmattan/README \
    qtc_packaging/debian_harmattan/copyright \
    qtc_packaging/debian_harmattan/control \
    qtc_packaging/debian_harmattan/compat \
    qtc_packaging/debian_harmattan/changelog \
    qtc_packaging/debian_fremantle/rules \
    qtc_packaging/debian_fremantle/README \
    qtc_packaging/debian_fremantle/copyright \
    qtc_packaging/debian_fremantle/control \
    qtc_packaging/debian_fremantle/compat \
    qtc_packaging/debian_fremantle/changelog \
    images/led-sep.png \
    images/led-9.png \
    images/led-8.png \
    images/led-7.png \
    images/led-6.png \
    images/led-5.png \
    images/led-4.png \
    images/led-3.png \
    images/led-2.png \
    images/led-1.png \
    images/led-0.png \
    Bedside.svg \
    Bedside_harmattan.desktop \
    android/res/values-es/strings.xml \
    android/res/values-it/strings.xml \
    android/res/values-pt-rBR/strings.xml \
    android/res/values-ms/strings.xml \
    android/res/drawable-mdpi/icon.png \
    android/res/values-ro/strings.xml \
    android/res/values-et/strings.xml \
    android/res/values/libs.xml \
    android/res/values/strings.xml \
    android/res/values-zh-rTW/strings.xml \
    android/res/values-de/strings.xml \
    android/res/values-nb/strings.xml \
    android/res/values-id/strings.xml \
    android/res/values-zh-rCN/strings.xml \
    android/res/drawable/logo.png \
    android/res/drawable/icon.png \
    android/res/drawable-ldpi/icon.png \
    android/res/values-pl/strings.xml \
    android/res/values-fa/strings.xml \
    android/res/layout/splash.xml \
    android/res/values-ru/strings.xml \
    android/res/drawable-hdpi/icon.png \
    android/res/values-el/strings.xml \
    android/res/values-rs/strings.xml \
    android/res/values-ja/strings.xml \
    android/res/values-nl/strings.xml \
    android/res/values-fr/strings.xml \
    android/version.xml \
    android/src/org/kde/necessitas/ministro/IMinistro.aidl \
    android/src/org/kde/necessitas/ministro/IMinistroCallback.aidl \
    android/src/org/kde/necessitas/origo/QtActivity.java \
    android/src/org/kde/necessitas/origo/QtApplication.java \
    android/AndroidManifest.xml

device|simulator {
  OBJECTS_DIR = $${DESTDIR}/.obj
  MOC_DIR = $${DESTDIR}/.moc
  RCC_DIR = $${DESTDIR}/.rcc
  UI_DIR = $${DESTDIR}/.ui

  suredelete.target = sureclean
  suredelete.commands = $(DEL_FILE) $${MOC_DIR}/*; $(DEL_FILE) $${RCC_DIR}/*; $(DEL_FILE) $${UI_DIR}/*
  suredelete.depends = distclean

  QMAKE_EXTRA_TARGETS += suredelete

#  TRANSLATIONS = \
#      $${TARGET}_en_GB.ts \
#      $${TARGET}_fr.ts \
#      $${TARGET}_it.ts \
#      $${TARGET}_de.ts \
#      $${TARGET}_es.ts \
#      $${TARGET}.ts
}


maemo5 {
    QT += dbus
    icon.files = Bedside.png
    icon.path = /usr/share/icons/hicolor/64x64/apps
    INSTALLS += icon
}

unix:!symbian:!maemo5 {
    icon.files = Bedside.svg
    icon.path = /usr/share/icons/hicolor/scalable/apps
    INSTALLS += icon

    CONFIG += meegotouch
}

RESOURCES += \
    assets.qrc









