import QtQuick 1.0
import com.nokia.meego 1.0
import QtMobility.sensors 1.2

PageStackWindow {
    id: window
    showStatusBar: false
    showToolBar: false

    initialPage: Page {
        id: page
        Rectangle {
            color: "#555555"
            anchors.fill: parent

            Main {
                id: root
                anchors.fill: parent

                states: State  {
                    name: "FLIPPED"
                    PropertyChanges { target: root; rotation: 180}
                }

                transitions: Transition {
                    NumberAnimation { properties: "rotation"; duration: 200; easing.type: Easing.InQuad }
                }
            }
        }
    }

    /**
     * Allow "upside down" display.
     */
    OrientationSensor {
        active: activeMonitor.active

        onReadingChanged: {
            if (reading.orientation == OrientationReading.LeftUp) {
                page.orientationLock = PageOrientation.LockLandscape
                root.state = 'FLIPPED'
            } else if (reading.orientation == OrientationReading.TopDown) {
                page.orientationLock = PageOrientation.LockPortrait
                root.state = 'FLIPPED'
            } else {
                page.orientationLock = PageOrientation.Automatic
                root.state = ''
            }

            console.log(reading.orientation + " at " + root.rotation)
        }
    }

    Component.onCompleted: {
        if (platformWindow)
            activeMonitor.external = true
        console.log("Set activeMonitor external modification = " + activeMonitor.external)
    }

    Connections {
        target: platformWindow
        onVisibleChanged: updateVisible()
        onActiveChanged: updateVisible()
    }

    function updateVisible() {
        console.log("PW Visible ["+ platformWindow.visible +"], active [" + platformWindow.active + "]")

        activeMonitor.visible = platformWindow.visible
        activeMonitor.active  = platformWindow.active
    }
}
