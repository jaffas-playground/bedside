import QtQuick 1.0

/**
 * Show an LED clock.
 *
 * @author Copyright (c) Andrew Flegg 2011. Released under the Artistic Licence.
 */
Rectangle {
    id: clock
    property bool active: true
    signal updated()

    anchors.fill: parent
    color: "black"
    
    Item {
        id: values

        property int    block_width: 204
        property double separator_width: 37
        property double padding_fraction: 0.5 - (4.0 * block_width + separator_width) / (854.0 * 2)
        property double scale: parent.width / (separator_width + (4.0 + 2 * padding_fraction) * block_width)
        property double digit_width: block_width * scale
    }


    LED {
        id: digit1
        x: values.padding_fraction * values.block_width * values.scale
    }

    LED {
        id: digit2
        x: digit1.x + digit1.width
    }

    LED {
        id: separator
        width: values.separator_width * values.scale
        value: 'sep'
        x: digit2.x + digit2.width
    }

    LED {
        id: digit3
        x: separator.x + separator.width
    }

    LED {
        id: digit4
        x: digit3.x + digit3.width
    }

    LED {
        id: ampmIndicator
        value: 'am'
        x: digit3.x + digit3.width + (ledWidth/2)
        y: 0 - digit3.ledHeight / 2 - ledHeight / 2
        visible: ampm
    }
    
    states: [
        State {
            name: "stacked"
            PropertyChanges {
                target: values
                block_width: 256
            }
            PropertyChanges {
                target: digit1
                y: 0
                x: values.padding_fraction
            }
            PropertyChanges {
                target: digit2
                y: 0
            }
            PropertyChanges {
                target: digit3
                x: digit1.x
                y: digit2.y + digit2.height
            }
            PropertyChanges {
                target: digit4
                x: digit2.x
                y: digit3.y
            }
            PropertyChanges {
                target: separator
                x: digit2.x + digit2.width
                y: digit3.y - separator.height / 2
            }
        }
    ]

    Rectangle {
        id: dimmingRectangle
        anchors.fill: parent
        color: "black"
        opacity: 0

        states {
            State {
                name: "dim"
                when: applicationData.dimmed
                PropertyChanges { target: dimmingRectangle; opacity: 0.66 }
            }
        }
    }

    function redraw() {
        var time = new Date()
        var hours   = time.getHours()
        var minutes = time.getMinutes()

        ampmIndicator.value = (ampm && hours >= 12) ? 'pm': 'am'
        if (ampm && hours > 12)
            hours -= 12
        
        digit1.value = (!ampm || hours >= 10) ? Math.floor(hours / 10) : 'blank'
        digit2.value = hours % 10
        digit3.value = Math.floor(minutes / 10)
        digit4.value = minutes % 10

        timer.interval = (60 - new Date().getSeconds()) * 1000
        clock.updated();
    }

    Component.onCompleted: redraw()
    onActiveChanged: if (active) redraw()

    Timer {
        id: timer
        interval: 60000
        repeat: true
        onTriggered : clock.redraw()
        running: clock.active
    }
}
