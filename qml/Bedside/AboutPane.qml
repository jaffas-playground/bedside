import QtQuick 1.0

Rectangle {
    id: aboutRoot
    width: parent.width
    height: panel.height * 1.1
    y: parent.height
    opacity: 0
    color: '#222222'

    Image {
        source: 'qrc:///assets/Bedside.png'
        width: 7 * screenScale
        height: 7 * screenScale
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.leftMargin: 5
        anchors.topMargin: 5
    }

    Column {
        id: panel
        width: parent.width
        spacing: 2 * screenScale
        Text {
            width: parent.width
            text: "Bedside"
            color: "white"
            font.pixelSize: 6 * screenScale
            font.bold: true
            horizontalAlignment: Text.AlignHCenter
        }
        Text {
            width: parent.width
            text: '<style>a:link{color:#FF0000}</style>Released under the <a href="http://www.perlfoundation.org/artistic_license_2_0">Artistic License</a>'
            textFormat: Text.RichText
            color: "white"
            font.pixelSize: 2.5 * screenScale
            horizontalAlignment: Text.AlignHCenter
            onLinkActivated: Qt.openUrlExternally(link)
            wrapMode: Text.WordWrap
        }
        Text {
            width: parent.width
            text: '<style>a:link{color:#FF0000}</style>Copyright &copy; <a href="mailto:andrew@bleb.org">Andrew Flegg</a> & <a href="mailto:andrew.olmsted@gmail.com">Andrew Olmsted</a> 2011'
            textFormat: Text.RichText
            color: "white"
            font.pixelSize: 2.5 * screenScale
            horizontalAlignment: Text.AlignHCenter
            wrapMode: Text.WordWrap
        }
        Text {
            width: parent.width
            text: '<b>Tip:</b> Swipe left/right horizontally to change colours<br/>Up/down to adjust brightness'
            textFormat: Text.RichText
            color: "white"
            font.pixelSize: 2.5 * screenScale
            horizontalAlignment: Text.AlignHCenter
            wrapMode: Text.WordWrap
        }
    }

    states: State {
        name: 'show'
        PropertyChanges { target: aboutPane; opacity: 0.95; y: root.height - aboutRoot.height }
        PropertyChanges { target: aboutTimer; running: true }
        PropertyChanges { target: hideAbout; enabled: true }
    }

    transitions: Transition {
        NumberAnimation { properties: "opacity"; duration: 500 }
        NumberAnimation { properties: "y"; duration: 500; easing.type: Easing.InOutBack }
    }

    Timer {
       id: aboutTimer
       interval: 30000
       running: false
       repeat: false
       onTriggered: aboutPane.state = ''
    }

    SmallButton {
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        text: 'x'
    }

    MouseArea {
        id: hideAbout
        x: 0; y: 0; z: 100
        height: aboutRoot.parent.height
        width: aboutRoot.parent.width
        enabled: false
        onClicked: aboutPane.state = ''
    }
}

