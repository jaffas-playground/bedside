import QtQuick 1.0
import QtMobility.systeminfo 1.2

Item {
    Main {}
    
    /**
     * Allows access to the screensaver of the device; allowing it to be
     * disabled when the app is in the foreground.
     */
    ScreenSaver {
        id: screenSaver
        screenSaverDelayed: activeMonitor.active
    }
}
    