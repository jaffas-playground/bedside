import QtQuick 1.0


/**
 * A simple display banner for application messages.
 */
Rectangle {
    id: infoBanner

    property string message: ""

    width: parent.width - 10
    anchors.horizontalCenter: parent.horizontalCenter
    height: 9 * screenScale
    radius: 2 * screenScale
    color: "black"
    y: -10 * screenScale

    Text {
        color: "white"
        font.pixelSize: 4 * screenScale
        horizontalAlignment: Text.AlignHCenter
        width: parent.width
        anchors.verticalCenter: parent.verticalCenter
        text: parent.message
        onTextChanged: {
            parent.y = 5
            timer.restart()
        }
    }

    Behavior on y {
        NumberAnimation { duration: 500; easing.type: Easing.InOutBack; onComplete: message = "" }
    }

    Timer {
        id: timer
        interval: 2000
        repeat: false
        onTriggered: parent.y = -10 * screenScale
        running: false
    }
}
