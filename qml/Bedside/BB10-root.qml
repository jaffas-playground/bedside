import QtQuick 1.0

Item {
    id: window
    
    Main {
        id: main
    }
    
    Connections {
        target: activeMonitor
        onChanged: updateVisible()
    }
    
    Connections {
        target: main
        onClockUpdated: updateVisible()
    }

    function updateVisible() {
        main.clockState = activeMonitor.active ? "" : 'stacked';
        timer.running = !activeMonitor.active;
    }
    
    
    Timer {
        id: timer
        interval: 100
        repeat: false
        onTriggered: activeMonitor.updateCover();
        running: false
    }
}
