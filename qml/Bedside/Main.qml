import QtQuick 1.0

Item {
    id: root
    anchors.fill: parent
    
    property alias clockState: clock.state
    signal clockUpdated()

    /**
     * The main element which does the work. Displays the time.
     */
    Clock {
        id: clock
        active: activeMonitor.visible

        MouseArea {
            property int startX;
            property int startY;

            anchors.fill: parent
            
            function updateDimmed(dim) {
                applicationData.dimmed = dim;
                infoBanner.message = "Backlight " + (applicationData.dimmed ? 'low' : 'high');
                backlight.setDimmed(applicationData.dimmed);
            }

            onPressed: {
                startX = mouse.x;
                startY = mouse.y;
            }
    
            onReleased: {
                var deltaX = mouse.x - startX;
                var deltaY = mouse.y - startY;
    
                if (deltaX > 100 && Math.abs(deltaY) < 100) {
                    var num = applicationData.colorNum + 1;
                    if (num > 6)
                        num = 0;
                    applicationData.colorNum = num;
                    
                } else if (deltaX < -100 && Math.abs(deltaY) < 100) {
                    var num = applicationData.colorNum - 1;
                    if (num < 0)
                        num = 6;
                    applicationData.colorNum = num
                    
                } else if (Math.abs(deltaX) < 100 && startY > 50 && deltaY < 100) {
                    updateDimmed(false);
                    
                } else if (Math.abs(deltaX) < 100 && startY > 50 && deltaY > 100) {
                    updateDimmed(true);
                }
            }

            onDoubleClicked: updateDimmed(!applicationData.dimmed)
        }
        
        onUpdated: root.clockUpdated()
    }

    /**
     * Display messages.
     */
    InfoBanner {
        id: infoBanner
    }

    /**
     * On Maemo 5, we want to show a "minimise" button.
     */
    HildonTaskButton {
        anchors.left: parent.left
        icon: 'wmTaskSwitcherIcon'

        onClicked: {
            viewer.minimise()
        }
    }

    /**
     * On Maemo 5, we want to show a "close" button.
     */
    HildonTaskButton {
        anchors.right: parent.right
        icon: 'wmCloseIcon'
        visible: needCloseIcon
        width: 50
        height: 50

        onClicked: Qt.quit()

        SmallButton {
            anchors.top: parent.top
            anchors.right: parent.right
            text: 'x'
            onClicked: Qt.quit()
        }
    }

    /**
     * An unobtrusive about button.
     */
    SmallButton {
        id: aboutButton
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        text: '?'
        onClicked: aboutPane.state = 'show'
    }

    AboutPane {
        id: aboutPane
    }
}
