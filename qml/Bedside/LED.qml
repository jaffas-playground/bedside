import QtQuick 1.0

/**
 * Show an image representing an LED segment.
 *
 * @author Copyright (c) Andrew Flegg 2011. Released under the Artistic Licence.
 */
Item {
    property string value: 'blank'
    height: ledImage.height
    width: values.digit_width
    y: (parent.height - ledImage.height) / 2
    property alias ledHeight: ledImage.height
    property alias ledWidth: ledImage.width

    Rectangle {
        width: ledImage.width -2
        height: ledImage.height -2
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        color: applicationData.color
    }
    Image {
        id: ledImage
        width: sourceSize.width * values.scale
        height: sourceSize.height * values.scale
        fillMode: Image.PreserveAspectFit
        smooth: true
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        source:  ':/assets/images/led-' + value + '.png'
    }
}
