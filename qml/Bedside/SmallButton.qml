import QtQuick 1.0

Rectangle {
    id: smallButton
    property string text
    signal clicked()

    width: 7 * screenScale
    height: 7 * screenScale
    color: '#222222'
    opacity: 0.5
    radius: width / 2

    Text {
        text: smallButton.text
        font.pixelSize: 5 * screenScale
        color: '#444444'
        anchors.centerIn: parent
    }

    MouseArea {
        anchors.fill: parent
        onClicked: smallButton.clicked()
    }
}
